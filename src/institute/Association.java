package institute;

import java.util.ArrayList;
import java.util.List;

public class Association {
    public static void main(String[] args) {
        Student s1= new Student("Zalak",1,"CST");
        Student s2=new Student("Neha",2,"BIO");
        Student s3= new Student("Aaska",3,"BIO");
        Student s4= new Student("Akshay",4,"CST");

        List<Student> cst_students = new ArrayList<Student>();
        cst_students.add(s1);
        cst_students.add(s4);

        List<Student> bio_students = new ArrayList<Student>();
        bio_students.add(s2);
        bio_students.add(s3);

        Department CST=new Department("CST",cst_students);
        Department BIO=new Department("BIO",bio_students);

        List<Department> departments = new ArrayList<Department>();
        departments.add(CST);
        departments.add(BIO);

        Institute institute=new Institute("IIT",departments);
        System.out.print("Total students in the institute: ");
        System.out.println(institute.getTotalStudentsInInstitute());

    }
}
