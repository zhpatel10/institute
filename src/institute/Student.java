package institute;

public class Student {
    String name;
    int id;
    String dept;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getDept() {
        return dept;
    }

    public Student(String name, int id, String dept) {
        this.name = name;
        this.id = id;
        this.dept = dept;


    }
}
